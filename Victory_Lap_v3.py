from keras.models import Sequential
from keras.layers import Activation, Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Lambda, Input
import tensorflow as tf
import numpy as np
import pandas as pd
import cv2 as cv
import os

def resize_output_shape(input_shape):
	return tuple([-1,40,80,3])

def preprocess(x):
    import tensorflow as tf
    x = tf.image.resize_images(x,(40,80))
    x = tf.map_fn(lambda img: tf.image.per_image_standardization(img), x)
    return x

def load_images(images):
	data = []
	for img in images:
		temp = cv.imread(img[0])
		data.append(temp)
	data = np.asarray(data)
	return data

########################################################################
df = pd.read_csv('beta_simulator_Data/drive_log/driving_log.csv')
df.columns = ['center', 'left', 'right', 'steering', 'throttle', 'brake', 'speed']
df = df.drop(columns = ['throttle', 'brake', 'speed'])
#X = df['center'].values
y = df['steering'].values
X = df[['center', 'left', 'right']].values
img_dir = '/home/anurag/beta_simulator_linux/beta_simulator_Data/drive_log/IMG'

################################################################################################################################


################################################################################################################################

def image_load_generator(images, steering_angles, batch_size):

    L = len(images)

    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)
            try:
                temp = images[batch_start:(batch_start+6),0].reshape(6,1)
                temp2 = images[(batch_start+6):(batch_start+12),1].reshape(6,1)
                temp3 = images[(batch_start+12):limit,2].reshape(4,1)
                t = np.concatenate((temp,temp2,temp3), axis=0) 
                X = load_images(t)
            except:
                X = load_images(images[batch_start:limit])
            Y = steering_angles[batch_start:limit]
            yield (X,Y)

            batch_start += batch_size   
            batch_end += batch_size

################################################################################################################################

def model_maker():
    model = Sequential()
    model.add(Lambda(preprocess, input_shape=(160,320,3), output_shape=resize_output_shape))
    model.add(Conv2D(32, 3, input_shape=(160,320,3), activation='elu', data_format = 'channels_last', use_bias=True))
    model.add(Conv2D(32, 3, activation='elu', use_bias=True))
    model.add(Conv2D(32, 3, activation='elu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    model.add(Conv2D(64, 3, activation='elu', use_bias=True))
    model.add(Conv2D(64, 3, activation='elu', use_bias=True))
    model.add(Dropout(0.1))
    model.add(Conv2D(64, 3, activation='elu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(50, activation='elu', use_bias=True))
    model.add(Dense(1))
    return model

################################################################################################################################

model = model_maker()
model.compile(loss='mse', optimizer='adam')

model.fit_generator(image_load_generator(X, y, 16), steps_per_epoch=X.shape[0]/16, epochs=10, verbose=1)

model.save('modelv3.h5')