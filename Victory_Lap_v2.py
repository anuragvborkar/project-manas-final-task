from keras.models import Sequential
from keras.layers import Activation, Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Lambda, Input

import numpy as np
import pandas as pd
import cv2 as cv
import os
from keras.preprocessing.image import ImageDataGenerator

def file_formatting(x):
    x = x.split('/')[-1]
    return x

df = pd.read_csv('beta_simulator_Data/drive_log/driving_log.csv')
df.columns = ['center', 'left', 'right', 'steering', 'throttle', 'brake', 'speed']
df = df.drop(columns = ['throttle', 'brake', 'speed'])
#X = df[['center', 'left', 'right']].values
X = df['center'].values
y = df['steering'].values
df['center'] = df['center'].apply(file_formatting)
img_dir = '/home/anurag/beta_simulator_linux/beta_simulator_Data/drive_log/IMG'
#######################################################################################
def resize_output_shape(input_shape):
	return tuple([-1,40,80,3])

def preprocess(x):
    import tensorflow as tf
    x = tf.image.resize_images(x,(40,80))
    x = tf.map_fn(lambda img: tf.image.per_image_standardization(img), x)
    return x

#######################################################################################

#center_images = np.load('center_images.npy')
#left_images = np.load('left_images.npy')
#right_images = np.load('right_images.npy')
#steering_angle = np.load('steering_angle.npy')

##############################################################################################
model = Sequential()
model.add(Lambda(preprocess, input_shape=(160,320,3), output_shape=resize_output_shape))
model.add(Conv2D(32, 3, activation='elu', data_format = 'channels_last', use_bias=True))
model.add(Conv2D(32, 3, activation='elu', use_bias=True))
model.add(MaxPooling2D(pool_size=2))

model.add(Conv2D(32, 3, activation='elu', use_bias=True))
model.add(MaxPooling2D(pool_size=2))

model.add(Conv2D(64, 3, activation='elu', use_bias=True))
model.add(Conv2D(64, 3, activation='elu', use_bias=True))
model.add(MaxPooling2D(pool_size=2))

model.add(Flatten())
model.add(Dense(50, activation='elu', use_bias=True))
model.add(Dense(1))

model.compile(loss='mean_squared_error', optimizer='adam')
##############################################################################################

datagen=ImageDataGenerator()
train_generator=datagen.flow_from_dataframe(dataframe=df, directory=img_dir, x_col='center', y_col='steering', target_size=(160,320), batch_size=16, class_mode='other')

model.fit_generator(generator=train_generator, steps_per_epoch=X.shape[0]/16, epochs=100)

model.save('modelv3.h5')