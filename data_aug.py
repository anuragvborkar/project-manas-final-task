import pandas as pd
import cv2 as cv
img_dir = '/home/anurag/beta_simulator_linux/beta_simulator_Data/drive_log/IMG/img_'

count=0
df = pd.read_csv('beta_simulator_Data/drive_log/driving_log.csv')
df.columns = ['center', 'left', 'right', 'steering', 'throttle', 'brake', 'speed']
df = df.drop(columns = ['left','right','throttle', 'brake', 'speed'])
y = df['steering'].values
X = df['center'].values

new_imgs = []

for i in range(X.shape[0]):
	if y[i]>0 or y[i]<0:
		img = cv.imread(X[i])
		flip = cv.flip(img,1)
		name = img_dir+str(count)+'.jpg'
		count+=1
		angle = -1*y[i]
		cv.imwrite(name,flip)
		new_imgs.append(pd.Series( [name, angle] , index=df.columns))

new_df = df.append(new_imgs , ignore_index=True)
new_df.to_csv('augmented.csv')