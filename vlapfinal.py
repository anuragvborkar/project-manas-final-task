from keras.models import Sequential
from keras.layers import Activation, Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Lambda, Input, Cropping2D, BatchNormalization
import tensorflow as tf
import numpy as np
import pandas as pd
import cv2 as cv
import os
import random
from random import randint

################################################################################################################################

def grayscale(x):
    import tensorflow as tf
    #import random
    #x = tf.image.rgb_to_hsv(x)
    '''if random.random()<0.2:
        tf.image.adjust_gamma(x)
    elif random.random()<0.4:
        tf.image.adjust_gamma(x)'''
    #x = tf.image.hsv_to_rgb(x)
    return tf.image.rgb_to_grayscale(x)

################################################################################################################################

def shadow(img,val):
    temp = np.copy(img)
    y1, y2, y3, y4 = randint(15,20), randint(55, 67), randint(55, 67), randint(15,35)
    x1, x2, x3, x4 = randint(140,150), randint(140,150),randint(155,180), randint(155,180)
    points = [np.array([[x1,y1],[x2,y2],[x3,y3],[x4,y4]], dtype=np.int32)]
    return cv.fillPoly(temp,points,val)

################################################################################################################################

def load_images(images):
    data = []
    for img in images:
        temp = cv.imread(img)
        '''if random.random()>0.9:
            temp = shadow(temp,50)'''
        data.append(temp)
    data = np.asarray(data)
    return data

################################################################################################################################

def file_formatting(x):
    x = x.split('/')[-1]
    x = '/media/morty/HPV175W/VictoryLap/IMG/'+x
    return x

################################################################################################################################

df = pd.read_csv('beta_simulator_Data/drive_log/augmented.csv')

'''GPU Alterations

df = pd.read_csv('/media/morty/HPV175W/VictoryLap/augmented.csv')
df['center'] = df['center'].apply(file_formatting)

'''

y = df['steering'].values
X = df['center'].values

################################################################################################################################

def image_load_generator(images, steering_angles, batch_size):

    L = len(images)

    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)
            Y = steering_angles[batch_start:limit]
            X = load_images(images[batch_start:limit])
            yield (X,Y)

            batch_start += batch_size
            batch_end += batch_size

################################################################################################################################

def mean_center(x):
    import tensorflow as tf
    return tf.map_fn(lambda img: tf.image.per_image_standardization(img), x)

################################################################################################################################

def model_maker():
    model = Sequential()
    model.add(Cropping2D(cropping=((64,20),(0,0)), input_shape=(160,320,3), data_format = 'channels_last'))
    model.add(Lambda(grayscale, input_shape=(76,320,3), output_shape=(76,320,1) ))
    model.add(Lambda(mean_center))

    model.add(Conv2D(16, 3, activation='relu', use_bias=True))
    model.add(MaxPooling2D(pool_size=(2,4)))
    
    model.add(Conv2D(32, 3, activation='relu', use_bias=True))
    model.add(MaxPooling2D(pool_size=(2,4)))
    
    model.add(Conv2D(32, 3, activation='relu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    
    model.add(Conv2D(64, 3, activation='relu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))

    model.add(Flatten())
    model.add(Dense(75, activation='elu'))
    model.add(Dropout(0.25))
    model.add(Dense(50, activation='elu', use_bias=True))
    model.add(Dense(1))
    return model

################################################################################################################################

model = model_maker()
model.compile(loss='mse', optimizer='adam')
#print(model.summary())
model.fit_generator(image_load_generator(X, y, 16), steps_per_epoch=X.shape[0]/16, epochs=350)
model.save('vlap30.h5')