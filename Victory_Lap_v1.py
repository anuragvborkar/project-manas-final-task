from keras.models import Sequential
from keras.layers import Activation, Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Lambda, Input
import tensorflow as tf
import numpy as np
import pandas as pd
import cv2 as cv
import os

df = pd.read_csv('beta_simulator_Data/drive_log/driving_log.csv')
df.columns = ['center', 'left', 'right', 'steering', 'throttle', 'brake', 'speed']
df = df.drop(columns = ['throttle', 'brake', 'speed'])
#X = df[['center', 'left', 'right']].values
X = df['center'].values
y = df['steering'].values
img_dir = '/home/anurag/beta_simulator_linux/beta_simulator_Data/drive_log/IMG'

#######################################################################################
def resize_output_shape(input_shape):
	return tuple([-1,40,80,3])

def resize_img(x):
	x = tf.image.resize_images(x,(40,80))
	return x
#######################################################################################

#center_images = np.load('center_images.npy')
#left_images = np.load('left_images.npy')
#right_images = np.load('right_images.npy')
#steering_angle = np.load('steering_angle.npy')

def load_images(images):
	data = []
	for img in images:
		temp = cv.imread(img)
		data.append(temp)
	data = np.asarray(data)
	return data

def image_load_generator(images, steering_angles, batch_size):

    L = len(images)

    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)
            X = load_images(images[batch_start:limit])
            Y = steering_angles[batch_start:limit]
            print(Y[0])

            yield (X,Y)

            batch_start += batch_size   
            batch_end += batch_size

##############################################################################################
model = Sequential()
#inp = Input(shape=(10,160,320,3))
model.add(Lambda(resize_img, input_shape=(160,320,3), output_shape=resize_output_shape))
model.add(Conv2D(32, 3, input_shape=(160,320,3), activation='elu', data_format = 'channels_last', use_bias=True))
model.add(Conv2D(32, 3, activation='elu', use_bias=True))
model.add(Conv2D(32, 3, activation='elu', use_bias=True))
model.add(MaxPooling2D(pool_size=2))
model.add(Conv2D(64, 3, activation='elu', use_bias=True))
model.add(Conv2D(64, 3, activation='elu', use_bias=True))
model.add(Dropout(0.1))
model.add(Conv2D(64, 3, activation='elu', use_bias=True))
model.add(MaxPooling2D(pool_size=2))
model.add(Flatten())
model.add(Dense(50, activation='elu', use_bias=True))
model.add(Dense(1))

model.compile(loss='mse', optimizer='adam')
##############################################################################################

#model.fit(center_images, steering_angle, batch_size=10, validation_split=0.1, epochs = 10)

model.fit_generator(image_load_generator(X, y, 20), steps_per_epoch=X.shape[0], epochs=1, verbose=1)

model.save('modelv2.h5')