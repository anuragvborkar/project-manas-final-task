from keras.models import Sequential
from keras.layers import Activation, Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Lambda, Input, Cropping2D, BatchNormalization
import tensorflow as tf
import numpy as np
import pandas as pd
import cv2 as cv
import os
import random

def grayscale(x):
    import tensorflow as tf
    return tf.image.rgb_to_yuv(x)

#############################################################################

def load_images(images):
    data = []
    for img in images:
        temp = cv.imread(img)
        data.append(temp)
    data = np.asarray(data)
    return data

########################################################################

df = pd.read_csv('beta_simulator_Data/drive_log/driving_log_adhi.csv')
df.columns = ['center', 'left', 'right', 'steering', 'throttle', 'brake', 'speed']
df = df.drop(columns = ['left','right','throttle', 'brake', 'speed'])
y = df['steering'].values
X = df['center'].values

################################################################################################################################


################################################################################################################################

def image_load_generator(images, steering_angles, batch_size):

    L = len(images)

    while True:

        batch_start = 0
        batch_end = batch_size

        while batch_start < L:
            limit = min(batch_end, L)
            Y = steering_angles[batch_start:limit]
            #if()
            X = load_images(images[batch_start:limit])
            yield (X,Y)

            batch_start += batch_size   
            batch_end += batch_size

################################################################################################################################

def model_maker():
    model = Sequential()
    model.add(Cropping2D(cropping=((64,20),(0,0)), input_shape=(160,320,3), data_format = 'channels_last'))
    model.add(Lambda(grayscale, input_shape=(76,320,3), output_shape=(76,320,3) ))
    model.add(Lambda(lambda x: (x/128.0-0.5)))
    model.add(Conv2D(32, 3, activation='elu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())
    model.add(Conv2D(32, 3, activation='elu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())
    model.add(Conv2D(32, 3, activation='elu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())
    model.add(Conv2D(64, 3, activation='elu', use_bias=True))
    model.add(MaxPooling2D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(50, activation='elu', use_bias=True))
    model.add(Dense(1))
    return model

################################################################################################################################

model = model_maker()
model.compile(loss='mse', optimizer='adam')

model.fit_generator(image_load_generator(X, y, 16), steps_per_epoch=X.shape[0]/16, epochs=10)

model.save('modelv4adhi.h5')