import pandas as pd
import cv2 as cv
import numpy as np
df = pd.read_csv('beta_simulator_Data/drive_log/driving_log.csv')
center_images = []
left_images = []
right_images = []
steering_angle = []

df.columns = ['center', 'left', 'right', 'steering', 'throttle', 'brake', 'speed']
df = df.drop(columns = ['throttle', 'brake', 'speed'])

training_data = []

for row in df.values:
    img_c = cv.imread(row[0])
    img_l = cv.imread(row[1])
    img_r = cv.imread(row[2])
    img_c = cv.resize(img_c, (80, 40))
    img_c, img_l, img_r = img_c/255.0, img_l/255.0, img_r/255.0
    img_c[:,:,0] = img_c[:,:,0]-np.mean(img_c[:,:,0])
    img_c[:,:,1] = img_c[:,:,1]-np.mean(img_c[:,:,1])
    img_c[:,:,2] = img_c[:,:,2]-np.mean(img_c[:,:,2])
    '''
    img_l[:,:,0] = img_l[:,:,0]-np.mean(img_l[:,:,0])
    img_l[:,:,1] = img_l[:,:,1]-np.mean(img_l[:,:,1])
    img_l[:,:,2] = img_l[:,:,2]-np.mean(img_l[:,:,2])
    
    img_r[:,:,0] = img_r[:,:,0]-np.mean(img_r[:,:,0])
    img_r[:,:,1] = img_r[:,:,1]-np.mean(img_r[:,:,1])
    img_r[:,:,2] = img_r[:,:,2]-np.mean(img_r[:,:,2])'''
    steer_angle = np.array([[row[3]]])
    training_data.append([img_c, img_l, img_r, steer_angle])

for center, left, right, angle in training_data:
    center_images.append(center)
    left_images.append(left)
    right_images.append(right)
    steering_angle.append(angle)
training_data = []
center_images = np.array(center_images).reshape(-1, 160, 320, 3)
'''left_images = np.array(left_images).reshape(-1, 160, 320, 3)
right_images = np.array(right_images).reshape(-1, 160, 320, 3)'''
steering_angle = np.array(steering_angle).reshape(-1, 1)

np.save('center_images.npy', center_images)
'''np.save('left_images.npy', left_images)
np.save('right_images.npy', right_images)'''
np.save('steering_angle.npy', steering_angle)